��    &      L  5   |      P     Q     l  "   �     �     �     �     �  	   �  
   �  "   �  "     (   <     e  	   r     |     �  
   �  
   �     �  
   �     �     �     �     �  	   �     �  
             %     4     A     J     Y  
   _     j  
   x     �  z  �          -  "   K     n     �     �     �     �     �  !   �  )     )   0     Z     h     w     �     �     �     �  	   �     �     �     �     	  	   	     	  
   	     '	     :	  
   I	     T	     \	     m	     {	     �	     �	     �	                       %                                            #                   &         "                 	          !                   $               
                       ({} action)  ({} actions)  ({} edit)  ({} edits)  UTC ({} action)  UTC ({} actions) Admin actions But nobody came Bytes changed Daily overview Day score Edits made Most active hour Most active hours Most active user Most active users Most edited article Most edited articles New articles New files No activity Unique contributors autopatrol autoreview bot bureaucrat century centuries day days decade decades director directors editor hour hours millennium millennia minute minutes month months reviewer second seconds sysop week weeks wiki_guardian year years {value} (avg. {avg}) Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-09-03 13:10+0200
Last-Translator: Eduaddad <>
Language-Team: Portuguese (Brazil) <https://weblate.frisk.space/projects/rcgcdw/main/pt_BR/>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Poedit 2.4.1
  ({} açao)  ({} ações)  ({} editado)  ({} edições)  UTC ({} ação)  UTC ({} ações) Ações de administração Mas ninguém veio Bytes alterados Visão geral diária Pontuação do dia Edições feitas Hora mais ativa Horas mais ativas Usuário mais ativo Usuários mais ativos Artigo mais editado Artigos mais editados Novos artigos Novos arquivos Sem atividade Contribuidores exclusivos patrulha automatica revisão automática robô burocrata século séculos dia dias década décadas diretor diretores editor hora horas milénio milénios minuto minutos mês meses revisor segundo segundos administrador semana semanas guardião_wiki ano anos {value} (med. {avg}) 