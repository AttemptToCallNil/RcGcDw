��    &      L  5   |      P     Q     l  "   �     �     �     �     �  	   �  
   �  "   �  "     (   <     e  	   r     |     �  
   �  
   �     �  
   �     �     �     �     �  	   �     �  
             %     4     A     J     Y  
   _     j  
   x     �  �  �  #   ]  &   �  /   �     �     �     	       
   /     :  S   L  a   �  d   	     g	     z	     �	     �	     �	     �	     �	  
   �	     �	     
     &
     :
  
   C
     N
     W
  %   n
     �
     �
     �
     �
     �
     �
          #     0                       %                                            #                   &         "                 	          !                   $               
                       ({} action)  ({} actions)  ({} edit)  ({} edits)  UTC ({} action)  UTC ({} actions) Admin actions But nobody came Bytes changed Daily overview Day score Edits made Most active hour Most active hours Most active user Most active users Most edited article Most edited articles New articles New files No activity Unique contributors autopatrol autoreview bot bureaucrat century centuries day days decade decades director directors editor hour hours millennium millennia minute minutes month months reviewer second seconds sysop week weeks wiki_guardian year years {value} (avg. {avg}) Project-Id-Version: RcGcDw
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-10-14 18:41+0000
Last-Translator: Rail <>
Language-Team: Polish <https://weblate.frisk.space/projects/rcgcdw/main/pl/>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Weblate 4.2.1
Generated-By: pygettext.py 1.5
  ({} akcja)  ({} akcje)  ({} akcji)  ({} edycja)  ({} edycje)  ({} edycji)  UTC ({} akcja)  UTC ({} akcje)  UTC ({} akcji) Akcji administratorskich Ale nikt nie przyszedł Zmienionych bajtów Podsumowanie dnia Wynik dnia Wykonanych edycji Najbardziej aktywna godzina Najbardziej aktywne godziny Najbardziej aktywne godziny Najbardziej aktywny użytkownik Najbardziej aktywni użytkownicy Najbardziej aktywni użytkownicy Najczęściej edytowany artykuł Najczęściej edytowane artykuły Najczęściej edytowane artykuły Nowych artykułów Nowych plików Brak aktywności Unikalnych edytujących Automatycznie zatwierdzający Automatycznie przeglądający Bot Biurokrata stulecie stulecia stuleci dzień dni dni dekada dekady dekad Dyrektor Dyrektorzy Redaktor godzina godziny godzin tysiąclecie tysiąclecia tysiącleci minuta minuty minut miesiąc miesiące miesięcy Przeglądający sekunda sekundy sekund Administrator tydzień tygodnie tygodni Strażnik wiki rok lata lat {value} (średnio {avg}) 